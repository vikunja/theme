const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
	content: ['./src/*.css', './layouts/**/*.html'],
	safelist: ['p-4', '!text-white', '!underline', 'bg-amber-400'],
	theme: {
		extend: {
			colors: {
				primary: {
					lighter: '#3b7aec',
					DEFAULT: '#196aff',
					darker: '#0064ff',
				},
			},
			fontFamily: {
				title: ['Quicksand', ...defaultTheme.fontFamily.sans],
				sans: ['Open Sans', ...defaultTheme.fontFamily.sans],
			},
			backgroundImage: {
				'hero-llama': "url('/images/bg-3.jpg')",
				'hero-llama-small': "url('/images/hero-llamas-small.jpg')",
				'hero-llama-2-small': "url('/images/hero-llamas-2-small.jpg')",
				'hero-llama-3-small': "url('/images/hero-llamas-3-small.jpg')",
			},
		},
	},
	plugins: [
		require('@tailwindcss/typography'),
		require('@tailwindcss/forms'),
		require('@tailwindcss/aspect-ratio'),
	],
};
